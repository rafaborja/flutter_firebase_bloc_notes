
import 'package:firebase_bloc_notes/models/note_model.dart';
import 'package:firebase_bloc_notes/repositories/base_repository.dart';

abstract class BaseNotesRepository extends BaseRepository {
  Future<Note> addNote({Note note});
  Future<Note> update({Note note});
  Future<Note> deleteNote({Note note});
  Stream<List<Note>> streamNotes({String userId});
}